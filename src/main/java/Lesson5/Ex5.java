// Napisz metodę, która jako jedyny argument przyjmie zmienną typu int[] i zwróci sumę wszystkich elementów tablicy
package Lesson5;

public class Ex5 {

    static int[] tablica = {2, 45, 98, 123547, 98752, 36598, 41598, 36947};

    public static void main(String[] args) {
        Ex5 n = new Ex5();
        n.sumaZtablicy(tablica);

    }

    private int sumaZtablicy(int[] tablica) {
        int i;
        int wynik = 0;
        for (i = 0; i < tablica.length; i++) {
            wynik += tablica[i];
        }
        System.out.println(wynik);
        return 0;
    }
}
