// Napisz pętlę while, która wypisze na ekranie wszystkie liczby od 10 do 20 włącznie
package Lesson5;

public class Ex2 {
    public static void main(String[] args) {
        int start = 10;
        int stop = 20;

        while (stop > start) {
            start++;
            System.out.println(start);
        }
    }
}
