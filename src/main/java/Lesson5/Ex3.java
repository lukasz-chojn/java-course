// Napisz pętlę for, która wypisze na ekranie wszystkie liczby nieparzyste od -10 do 40
package Lesson5;

public class Ex3 {
    public static void main(String[] args) {
        int start = -11;
        int stop = 41;
        int i;

        for (i = start; i < stop; i += 2) {
            System.out.println(i);
        }
    }
}
