// Napisz program, który wypisze na ekranie malejąco wszystkie liczby od 20 do 10
package Lesson5;

public class Ex1 {
    public static void main(String[] args) {
        int stop = 10;

        for (int i = 20; i >= stop; i--) {
            System.out.println(i);
        }
    }
}
