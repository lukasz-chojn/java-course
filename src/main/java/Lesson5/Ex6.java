// Przerób funkcję z zadania piątego tak, żeby metoda przyjmowała tablicę dwuwymiarową typu int[][]
package Lesson5;

public class Ex6 {

    static int[][] tablica = {{2, 52, 74, 58, 658, 41257, 42578}, {54, 52417, 6589, 2147125, 326589}};


    public static void main(String[] args) {
        Ex6 n = new Ex6();
        n.sumaZtablicy(tablica);
    }

    private int sumaZtablicy(int[][] tablica) {
        int i;
        int j;
        int result = 0;
        for (i = 0; i < tablica.length; i++) {
            for (j = 0; j < tablica[i].length; j++) {
                result = result + tablica[i][j];
            }
        }
        System.out.println("Suma wszystkich elementów z tablicy to: " + result);
        return 0;
    }
}
