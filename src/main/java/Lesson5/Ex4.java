
// Przerób pętlę z zadania trzeciego na pętlę while
package Lesson5;

public class Ex4 {
    public static void main(String[] args) {
        int start = -12;
        int stop = 40;

        while (start < stop) {
            start = start + 2;
            System.out.println(start);
        }
    }
}
