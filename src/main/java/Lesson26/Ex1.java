/*
Napisz program, który pobierze o użytkownika cztery łańcuchy znaków, które umieścisz w liście.
Następnie posortuj tę listę używając metody sort.
Użyj wyrażenia lambda, które posortuje łańcuchy znaków malejąco po długości.
 */
package Lesson26;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Ex1 {
    private static List<String> listOfStrings = new ArrayList<>();

    public static void main(String[] args) {
        Ex1 k = new Ex1();
        k.collectingWords();
        k.sortingList(listOfStrings);
        k.display(listOfStrings);
    }

    private void collectingWords() {
        String word = "";
        int number = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj cztery wyrazy: ");
        while (number < 4) {
            word = sc.next();
            listOfStrings.add(word);
            number++;
        }
    }

    private List<String> sortingList(List<String> listOfStrings) {
        Collections.sort(listOfStrings, (o1, o2) -> o1.length() - o2.length());
        return listOfStrings;
    }

    private void display(List<String> listOfStrings) {
        for (String after : listOfStrings) {
            System.out.println(after);
        }
    }
}
