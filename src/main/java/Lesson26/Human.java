/*
Utwórz instancję klasy Human przy pomocy mechanizmu odwoływania się do konstruktora (przy pomocy ::).
 */
package Lesson26;

import java.util.function.BiFunction;

public class Human {
    private int age;
    private String name;

    public Human(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public static void main(String[] args) {
        BiFunction<Integer, String, Human> human = Human::new;
        Human human1 = human.apply(33, "Łukasz");
        System.out.println(human1.getName() + ", " + human1.getAge());
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

}
