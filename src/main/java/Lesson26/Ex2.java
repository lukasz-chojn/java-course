/*
Napisz program, który wywoła funkcję equals na instancji klasy Object używając mechanizmu odwoływania się do metody (przy pomocy ::).
 */
package Lesson26;

import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Ex2 {
    public static void main(String[] args) {
        Supplier<Object> supplier = Object::new;
        Object objectInstance = supplier.get();
        System.out.println(objectInstance);

        BiPredicate<Object, Object> objectObjectBiPredicate = Object::equals;
        System.out.println(objectObjectBiPredicate.test(objectInstance, new Object()));

        Predicate<Object> objectPredicate = objectInstance::equals;
        System.out.println(objectPredicate.test(new Object()));
    }
}
