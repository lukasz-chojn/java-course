/*
Napisz zestaw testów jednostkowych potwierdzających poprawne działanie Twojego koszyka z zakupami.
 */
package Lesson22.test;

import Lesson22.Basket;
import Lesson22.Products;
import org.junit.Before;
import org.junit.Test;

public class BasketTest {
    private Basket basket;
    private Products product;

    @Before
    public void settingUp() {
        product = new Products("car", 48.65);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldAmountBiggerThanZeroOrMinusNumber() {
        basket.adding(product, 0);
        basket.adding(product, -2);
    }

    @Test
    public void shouldAddProductToBasket() {
        basket.adding(product, 2);
   /*     Map<Products, Integer> order = createOrder(product, 2);
        Assert.assertEquals(order, basket.getOrder());*/
    }

    @Test
    public void shouldRemoveFromBasket() {
        basket.adding(product, 5);
        basket.remove(product, 1);

    /*    Map<Products, Integer> order = createOrder(product, 1);
        Assert.assertEquals(order, basket.getOrder());*/
    }
}
