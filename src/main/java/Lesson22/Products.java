package Lesson22;

public class Products {
    private String productName;
    private double priceOfProduct;

    public Products(String productName, double priceOfProduct) {
        this.productName = productName;
        this.priceOfProduct = priceOfProduct;
    }

    public String getProductName() {
        return productName;
    }

    public double getPriceOfProduct() {
        return priceOfProduct;
    }

}
