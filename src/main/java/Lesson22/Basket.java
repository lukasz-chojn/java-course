/*
Napisz program, który będzie reprezentował koszyk w sklepie internetowym.
Do koszyka reprezentowanego przez klasę Basket możemy dodawać bądź usuwać kolejne przedmioty.
Każdy przedmiot powien mieć nazwę i cenę jednostkową.
Koszyk powinien także pozwalać na dodanie/usunięcie od razu kilku egzemplarzy przedmiotu ze sklepu.
Koszyk powinien także być w stanie policzyć sumaryczną wartość zamówienia oraz wyświetlić swoją zawartość.
Pamiętaj o poprawnym obsłużeniu sytuacji wyjątkowych np. usunięcie elementów z pustego koszyka czy dodaniu ujemej liczby przedmiotów.
 */
package Lesson22;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Basket {
    private Map<Products, Integer> listOfProducts = new HashMap<>();

    public void adding(Products product, Integer amountOfProducts) {
        addingToBasket(product, 1);
    }

    public void veryfingAmountOfProduct(Integer amountOfProduct) {
        if (amountOfProduct <= 0) {
            throw new IllegalArgumentException("Błędna ilość produktów w koszyku. Sprawdź koszyk");
        }
    }

    public void addingToBasket(Products product, Integer amountOfProduct) {
        veryfingAmountOfProduct(amountOfProduct);
        if (listOfProducts.containsKey(product)) {
            amountOfProduct = listOfProducts.put(product, amountOfProduct) + 1;
        }
        listOfProducts.put(product, amountOfProduct);
    }

    public void remove(Products product, Integer amountOfProducts) {
        remove(product, 1);
    }

    public void removeFromBasket(Products product, Integer amountOfProduct) {
        veryfingAmountOfProduct(amountOfProduct);
        if (listOfProducts.containsKey(product)) {
            amountOfProduct = listOfProducts.get(product) - amountOfProduct;
        }
    }

    public double getOrderPrice() {
        double orderPrice = 0;
        for (Map.Entry<Products, Integer> price : listOfProducts.entrySet()) {
            orderPrice += price.getKey().getPriceOfProduct() * price.getValue();
        }
        return orderPrice;
    }

    public Map<Products, Integer> getOrder() {
        return Collections.unmodifiableMap(listOfProducts);
    }
}
