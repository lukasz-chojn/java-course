/*
Ćwiczenie. Napisz metodę, która sprawdzi czy temperatura przekazana jako argument jest dodatnia.
 */

package Lesson1;

public class Ex1 {

    public static void main(String[] args) {

        int temperaturaDodatnia = 30;
        sprawdzWartosc(temperaturaDodatnia);
    }

    private static int sprawdzWartosc(int temperaturaDodatnia) {
        if (temperaturaDodatnia > 0) {
            System.out.println("Temperatura jest większa od 0 i wynosi " + temperaturaDodatnia + " stopni");

        } else {
            System.out.println("Temperatura jest mniejsza od 0");
        }
        return temperaturaDodatnia;
    }
}
