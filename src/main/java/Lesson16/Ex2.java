/*
Napisz program, który poprosi użytkownika nazwę pliku wyjściowego oraz o podanie swojej daty urodzenia
(osobno dzień, miesiąc i rok) a następnie zapisze te dane jako trzy osobne liczby binarnie.
Napisz program, który pobierze od użytkownika ścieżkę do pliku binarnego z datą urodzenia a następnie wyświetli ją na ekranie.
 */
package Lesson16;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Ex2 {
    private String filePath;

    Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        Ex2 k = new Ex2();
        k.receivingFilePath();
        k.inputBinary();
    }

    private String receivingFilePath() {
        System.out.print("Podaj ścieżkę do pliku, który chcesz zapisać: ");
        filePath = sc.next();
        return filePath;
    }

    private void inputBinary() throws IOException {
        int day, month, year;
        DataOutputStream binary = null;
        try {
            binary = new DataOutputStream(new FileOutputStream(filePath));
            System.out.print("Wprowadź datę urodzenia, najpierw dzień, potem miesiąc i na końcu rok: ");
            day = sc.nextInt();
            month = sc.nextInt();
            year = sc.nextInt();
            binary.writeInt(day);
            binary.writeInt(month);
            binary.writeInt(year);
        } finally {
            if (binary != null) {
                binary.close();
            }
        }
        System.out.print("Urodziłeś się w dniu: " + day + " miesiącu: " + month + "roku: " + year);
    }
}
