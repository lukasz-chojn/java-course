/*
Napisz program, który pobierze od użytkownika ścieżkę do pliku tekstowego oraz kilka linijek tekstu
(dopóki użytkownik nie wprowadzi „-” jako linijki) i zapisze je do pliku tekstowego.
Napisz program, który pobierze od użytkownika ścieżkę do pliku i wyświetli zawartość pliku na ekranie wraz z informacją ile linii znajduje się w pliku.
 */
package Lesson16;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Ex1 {
    private String filePath;
    Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        Ex1 j = new Ex1();
        j.receivingFilePath();
        j.fileWrite();
        j.fileRead();

    }

    private String receivingFilePath() {
        System.out.print("Podaj ścieżkę do pliku, który chcesz zapisać: ");
        filePath = sc.next();
        return filePath;
    }

    private void fileWrite() throws IOException {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(filePath);
            System.out.print("Wprowadź tekst, który chcesz zapisać: ");
            while (true) {
                String textToWrite = sc.nextLine();
                if (textToWrite.equals("-")) {
                    System.out.println("Zakończyłeś wprowadzanie tekstu");
                    break;
                }
                fileWriter.write(textToWrite);
                fileWriter.write(System.lineSeparator());
            }
        } finally {
            if (fileWriter != null) {
                fileWriter.close();
            }
        }
    }

    private void fileRead() throws IOException {
        BufferedReader reader = null;
        int numberOfLines = 0;
        try {
            reader = new BufferedReader(new FileReader(filePath));
            while (true) {
                String lineValue = reader.readLine();
                if (lineValue == null) {
                    break;
                }
                numberOfLines++;
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        System.out.println("Plik w lokalizacji " + filePath + " zawiera " + numberOfLines + " lini tekstu");
    }
}
