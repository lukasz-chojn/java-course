/*
Plik CSV zawiera trzy kolumny oddzielone znakiem ,.
Pierwsza kolumna zawiera imię, druga liczbę zmiennoprzecinkową trzecia dzień tygodnia.
Twoim zadaniem jest wczytanie zawartości tego pliku i wyświetlenie jej w formie tabeli.
 */
package Lesson25;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Ex1 {

    public static void main(String[] args) throws IOException {
        Ex1 k = new Ex1();
        k.readCSV();
    }

    private void readCSV() throws IOException {
        File file = new File("C:\\Users\\Łukasz\\Desktop\\test.csv");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        try {
            while (true) {
                String text = reader.readLine();
                if (text == null) {
                    break;
                }
                String[] str = text.split(",");
                System.out.print("| ");
                System.out.format("%7s", str[0]);
                System.out.print(" | ");
                System.out.format("%7.2f", Float.valueOf(str[1]));
                System.out.print(" | ");
                System.out.format("%12s", str[2]);
                System.out.println(" |");
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }
}
