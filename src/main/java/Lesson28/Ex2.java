/*
Znajdź minimalny element w kolekcji używając strumieni i funkcji reduce.
Twoja funkcja powinna działać jak istniejąca funkcja min.
 */
package Lesson28;

import java.util.Arrays;
import java.util.List;

public class Ex2 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(5, 6, 85, 21448, 3578, 324826, 32548);
        int min = numbers.get(0);
        numbers.stream().reduce(min, (x, y) -> x.compareTo(y) <= 0 ? x : y);
        System.out.println(min);
    }
}
