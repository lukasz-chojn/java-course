/*
Używając metody flatMap napisz strumień, który “spłaszczy” listę list.
 */
package Lesson28;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Ex3 {
    public static void main(String[] args) {
        List<List<String>> lists = Arrays.asList(
                Arrays.asList("hbububo", "1449", "ijiomom"),
                Arrays.asList("pkjhbhbhjvgyvgvycyvgcgffdxfxzs", "789547", "hbhftrdrfyhuihugyu")
        );
        lists.stream().flatMap(Collection::stream);
        lists.forEach(System.out::println);
    }
}
