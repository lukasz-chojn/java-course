/*
Przerób poniższa fragment kodu tak żeby używał strumieni
 */
package Lesson28;

import java.util.Comparator;

public class Ex1 {
    private void method() {
        BoardGame bestGame = null;
        double highestRanking = 0;
        for (BoardGame game : BoardGame.GAMES) {
            if (game.name.contains("a")) {
                if (game.rating > highestRanking) {
                    highestRanking = game.rating;
                    bestGame = game;
                }
            }
        }
        System.out.println(bestGame.name);
    }

    private void stream() {
        BoardGame bestGame = BoardGame.GAMES
                .stream()
                .filter(g -> g.name.contains("a"))
                .max(Comparator.comparingDouble(g1 -> g1.rating)).get();
        System.out.println(bestGame.name);
    }
}
