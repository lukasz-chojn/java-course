/*
Napisz program pobierający od użytkownika dwie liczby całkowite. Wyświetl wynik ich dzielenia wraz z częścią ułamkową.
 */
package Lesson11;

import java.util.Scanner;

public class Ex2 {

    static int firstUserNumber;
    static int secondUserNumber;
    int userNumbers[];

    public static void main(String[] args) {
        Ex2 k = new Ex2();
        k.getNumbers();
        k.Calculation(firstUserNumber, secondUserNumber);

    }

    private int[] getNumbers() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Podaj liczbę: ");
        firstUserNumber = sc.nextInt();
        System.out.print("Podaj liczbę: ");
        secondUserNumber = sc.nextInt();
        return userNumbers;
    }

    private void Calculation(int firstUserNumber, int secondUserNumber) {
        double result = (double) firstUserNumber / secondUserNumber;
        int mod = (int) ((int) firstUserNumber % secondUserNumber);
        if (mod > 0) {
            System.out.println("Reszta z dzielenia: " + mod);
            System.out.print("Wynik: " + result);
        } else {
            double result1 = (double) result;
            System.out.print("Wynik: " + result1);
        }
    }
}
