/*
Jak myślisz co otrzymasz przypisując zmienną typu char do zmiennej typu int? Znajdziesz ten numer w tabeli ASCII?
 */

package Lesson11;

public class Ex4 {
    public static void main(String[] args) {
        char a = 'c';
        int b = a;
        System.out.println(b);
    }
}
