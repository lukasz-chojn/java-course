/*
Napisz program, który skończy się wyjątkiem spowodowanym błędem podczas boxingu/unboxingu.
 */
package Lesson11;

public class Ex3 {
    public static void main(String[] args) {
        Double d1 = null;
        double d2 = d1;

        System.out.println(d2);
    }
}
