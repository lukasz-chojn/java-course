/*
Napisz program przyjmujący od użytkownika liczbę całkowitą i wyświetl wynik mnożenia tej liczby oraz stałej pi (Math.PI).
Wyświetl wynik w postaci liczby całkowitej i liczby zmiennoprzecinkowej.
 */
package Lesson11;

import java.util.Scanner;

public class Ex1 {

    static int userNumber;
    double pi = Math.PI;

    public static void main(String[] args) {
        Ex1 k = new Ex1();
        k.getNumber();
        k.Calculation(userNumber);

    }

    private int getNumber() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Podaj liczbę ");
        userNumber = sc.nextInt();
        return userNumber;
    }

    private void Calculation(int userNumber) {
        double result = userNumber * pi;
        int result1 = (int) result;
        System.out.print("Wynik z liczby zmiennoprzecinkowej: " + result + "\n");
        System.out.print("Wynik z liczby całkowitej: " + result1);
    }
}
