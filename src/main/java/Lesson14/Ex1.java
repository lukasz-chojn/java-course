/*
Napisz klasę reprezentującą człowieka, zaimplementuj metody hashCode i equals.
Zastanów się czy to, że ktoś ma to samo imię i nazwisko sprawia, że jest to ta sama osoba?
Jaki atrybut może posłużyć do sprawdzenia czy dwie instancje klasy Human reprezentują tę samą osobę?
 */
package Lesson14;

public class Ex1 {
    public static void main(String[] args) {
        Human h1 = new Human();
        Human h2 = new Human();
        h1.Human("Łukasz", "Chojnowski", "14270702193");
        h2.Human("test", "testowy", "85072310686");
        System.out.println(h1 + " == " + h2 + " " + h1.equals(h2));

    }
}

class Human {
    String name;
    String surname;
    String pesel;

    public void Human(String name, String surname, String pesel) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
    }

    @Override
    public int hashCode() {
        return 7 * pesel.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Object)) {
            return false;
        }
        Human another = (Human) obj;
        return pesel.equals(another.getPesel());
    }

    @Override
    public String toString() {
        return name + " " + surname + " " + pesel;
    }

    public String getPesel() {
        return pesel;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
}
