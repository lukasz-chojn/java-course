/*
Napisz program, w którym zasymulujesz hierarchię dziedziczenia zwierząt. Stwórz abstrakcyjną klasę Animal, po której będą dziedziczyły klasy Fish i Mammal.
Wszystkie te klasy powinny być abstrakcyjne. Następnie stwórz konkretne klasy które dziedziczą po Fish i Mammal. Będą to odpowiednio Goldfish i Human.
Nadpisz metodę toString w każdej z tych klas. Stwórz instancje obu tych klas i wyświetl je na konsoli.
 */
package Lesson8;

public class Ex1 {

    public static void main(String[] args) {
        Animal human = new Human();
        Fish fish = new Goldfish();

        System.out.println(fish.toString() + "\n" + human.toString());
    }
}
