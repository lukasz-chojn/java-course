/*
 Ma to być program, który przechowuje oceny jednego ucznia. Poniższe punkty opisują wymagania:

Jako nauczyciel chcę dodawać przedmioty do dzienniczka,
jako nauczyciel chcę dodać ocenę dla jednego z przedmiotów,
jako nauczyciel chcę policzyć średnią ocen dla danego przedmiotu,
jako nauczyciel chcę policzyć średnią ocen z wszystkich przedmiotów.
 */
package Lesson23;

public class StudentsDiary {

    public static void main(String[] args) throws Exception {
        Subjects subjects = new Subjects();
        subjects.collectingSubject();
        subjects.collectingNotes();
        subjects.print();
    }
}
