package Lesson23;

import java.util.*;

public class Subjects {
    private Map<String, List<Double>> notesList = new HashMap<>();
    Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        Subjects s = new Subjects();
        s.collectingSubject();
        s.collectingNotes();
        s.print();
    }

    protected void collectingSubject() throws Exception {
        String subject;
        System.out.println("Podaj nazwę przedmiotu, który chcesz wpisać do dzienniczka. Litera " + "k" + " kończy wpisywanie");
        while (true) {
            System.out.print("Przedmiot: ");
            subject = sc.next();
            if (!subject.matches("[A-Za-z]")) {
                throw new Exception("Nazwa przedmiotu to litery a nie cyfry");
            }
            if (subject.equals("k")) {
                break;
            }
            notesList.put(subject, new ArrayList<>());
        }
    }

    protected Map<String, List<Double>> collectingNotes() throws Exception {
        double note = 0;
        System.out.println("Podaj oceny, które chcesz wpisać do dzienniczka.\nCyfra " + "0" + " kończy wpisywanie");
        for (Map.Entry<String, List<Double>> sth : notesList.entrySet()) {
            System.out.print("Podaj oceny dla przedmiotu " + sth.getKey() + "\n");
            while (true) {
                System.out.print("Ocena: ");
                try {
                    note = sc.nextDouble();
                } catch (InputMismatchException e) {
                    System.out.println("Ocena musi być cyfrą. Litery są niedozwolone");
                }
                if (note < 0 || note > 6) {
                    throw new Exception("Podaj prawidłową wartość oceny! Ocena powinna zawierać się w przedziale od 1,0 do 6,0.");
                }
                if (note == 0) {
                    break;
                }
                sth.getValue().add(note);
            }
        }
        return notesList;
    }

    private double notesAverage(List<Double> values) {
        double average;
        double d = 0;
        for (Double value : values) {
            d = d + value;
        }
        average = d / values.size();
        return average;
    }

    private double averageFromAllNotes() {
        List<Double> all = new ArrayList<>();
        for (List<Double> valueList : notesList.values()) {
            all.addAll(valueList);
        }
        return notesAverage(all);
    }

    protected void print() {
        for (Map.Entry<String, List<Double>> map : notesList.entrySet()) {
            System.out.println("Przedmiot: " + map.getKey() + "\nŚrednia: " + notesAverage(map.getValue()) + "\nŚrednia ze wszystkich ocen: "
                    + averageFromAllNotes() + "\nOceny: " + map.getValue());
        }
    }
}
