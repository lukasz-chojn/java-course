//Napisz program pobierający trójelementową tablicę liczb i zwracający największą liczbę

package Lesson4;

import java.util.Arrays;
import java.util.Collections;

public class Ex2 {
    public static void main(String[] args) {
        Ex2 m = new Ex2();
        m.maxNumbers();
    }


    private void maxNumbers() {
        Double[] numbers = {20.586, 45.698, 36.5478};
        double result = Collections.max(Arrays.asList(numbers));
        System.out.println(result);
    }
}

