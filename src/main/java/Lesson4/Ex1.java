// Napisz metodę pobierającą dwuelementową tablicę liczb i zwracający ich sumę
package Lesson4;

public class Ex1 {
    public static void main(String[] args) {
        Ex1 n = new Ex1();
        n.sumNumbers();
    }

    private void sumNumbers() {
        int[] numbers = {12, 36};
        int result;
        result = numbers[0] + numbers[1];
        System.out.println(result);
    }
}
