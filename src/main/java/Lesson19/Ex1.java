/*
Napisz klasę Human, która będzie reprezentowała człowieka. Niech klasa ta posiada atrybuty takie jak imię, wiek, kolor oczu, kolor włosów.
Niech te dwa ostatnie atrybuty będą typami wyliczeniowymi. Stwórz instancję takiej klasy.
 */
package Lesson19;

public class Ex1 {
}

class Human {
    public static void main(String[] args) {
        Human human = new Human("Łukasz", 33, HairColor.BLACK, ColorOfEyes.BLUE);
        System.out.print(human.getName() + "\n" + human.getAge() + "\n" + human.getHairColor() + "\n" + human.getColorOfEyes());
    }

    private String name;
    private int age;
    private static HairColor HairColor;
    private static ColorOfEyes ColorOfEyes;

    public static enum HairColor {
        BLACK,
        BLONDE,
        RED
    }

    public enum ColorOfEyes {
        GREEN,
        BLUE,
        BLACK
    }

    public Human(String name, int age, HairColor hairColor, ColorOfEyes colorOfEyes) {
        this.name = name;
        this.age = age;
        this.HairColor = hairColor;
        this.ColorOfEyes = colorOfEyes;
    }

    private String getName() {
        return name;
    }

    private int getAge() {
        return age;
    }

    private HairColor getHairColor() {
        return HairColor;
    }

    private ColorOfEyes getColorOfEyes() {
        return ColorOfEyes;
    }
}
