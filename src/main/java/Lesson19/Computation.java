/*
Napisz typ wyliczeniowy Computation, który będzie reprezentował prosty kalkulator.
Niech typ ten posiada następujące wartości MULTIPY, DIVIDE, ADD, SUBTRACT.
Niech typ ten posiada metodę public double perform(double x, double y), która zwróci wynik odpowiedniej operacji.
 */
package Lesson19;

public enum Computation {
    MULTIPY {
        public double perform(double x, double y) {
            return x * y;

        }
    },
    DIVIDE {
        public double perform(double x, double y) {
            return x / y;

        }
    },
    ADD {
        public double perform(double x, double y) {
            return x + y;
        }

    },
    SUBTRACT {
        public double perform(double x, double y) {
            return x - y;

        }
    };

    public abstract double perform(double x, double y);

    public static void main(String[] args) {
        System.out.println("10 + 10 = " + Computation.ADD.perform(10, 10));
        System.out.println("7 - 8 = " + Computation.SUBTRACT.perform(7, 8));
        System.out.println("8 * 8 = " + Computation.MULTIPY.perform(8, 8));
        System.out.println("27 / 9 = " + Computation.DIVIDE.perform(27, 9));
    }
}
