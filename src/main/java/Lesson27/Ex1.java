/*
Napisz program, który będzie symulował działanie banku.
 */
package Lesson27;

public class Ex1 implements BankTransfer {
    public static final int CHARGE = 1;

    public static void main(String[] args) {
        Ex1 bank = new Ex1();
        BankAcount bankAcount1 = bank.open();
        BankAcount bankAcount2 = bank.open();

        bankAcount1.deposit(1000);
        bankAcount2.deposit(356);

        bank.transfer(bankAcount1, bankAcount2, 65);

        System.out.println(bankAcount1.getBalance());
        System.out.println(bankAcount2.getBalance());
    }

    public BankAcount open() {
        return new BankAcount();
    }

    @Override
    public void transfer(BankAcount from, BankAcount to, int amount) {
        if ((from.balance < amount + CHARGE)) {
        }
        from.withdraw(amount + CHARGE);
        to.deposit(amount);
    }
}
