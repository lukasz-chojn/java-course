package Lesson27;

class BankAcount implements Acount {

    int balance;

    BankAcount() {
        this(0);
    }

    BankAcount(int balance) {
        this.balance = balance;
    }

    @Override
    public void deposit(int amount) {
        if (amount <= 0) {
            System.out.println("Brak gotówki");
            System.exit(0);
        }
        balance += amount;
    }

    @Override
    public void withdraw(int amount) {
        if (amount <= 0) {
            System.out.println("Brak gotówki");
            System.exit(0);
        }
        balance -= amount;
    }

    public int getBalance() {
        return balance;
    }
}
