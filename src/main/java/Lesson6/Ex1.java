/*
Napisz program, który policzy średnią z kilku przedmiotów.
Możemy założyć, że uczeń w szkole ma 3 przedmioty i z każdego z nich dostał po 4 oceny
*/
package Lesson6;

import java.util.Scanner;

class Ex1 {
    private String name;
    private String[] subjects = new String[3];
    private float[][] notes = new float[3][4];

    Scanner sc = new Scanner(System.in);

    private void PobierzImieOdUsera() {
        System.out.print("Podaj imię ucznia: ");
        name = sc.next();
    }

    private void PobierzPrzedmiotyIocene() {
        for (int i = 0; i < subjects.length; i++) {
            System.out.print("Podaj nazwy przedmiotów, których uczy się " + name + " ");
            subjects[i] = sc.next();
            for (int j = 0; j < notes[i].length; j++) {
                System.out.print("Podaj ocenę dla przedmiotu: " + subjects[i] + " ");
                notes[i][j] = sc.nextFloat();
            }
        }
    }

    private void SrednieOcen() {
        float subjectAverage[] = new float[3];
        for (int i = 0; i < subjectAverage.length; i++) {
            float average = 0;

            for (int j = 0; j < notes[i].length; j++)
                average += notes[i][j];

            subjectAverage[i] = average / notes[i].length;
            System.out.print("Uczeń " + name + " z przedmiotu " + subjects[i] + " uzyskał następującą średnią ocen: " + subjectAverage[i] + " \n");
            sc.nextLine();
            sc.nextLine();
        }

    }


    public static void main(String[] args) {
        Ex1 n = new Ex1();
        n.PobierzImieOdUsera();
        n.PobierzPrzedmiotyIocene();
        n.SrednieOcen();
    }
}
