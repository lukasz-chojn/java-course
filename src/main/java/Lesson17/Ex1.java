/*
Zaimplementuj klasę HtmlTag, która będzie menadżerem kontekstu. W konstruktorze powinna przyjmować nazwę znacznika np. em.
Klasa powinna mieć metodę body, która przyjmie parametr typu String i wypisze go na konsoli.
 */
package Lesson17;

public class Ex1 implements AutoCloseable {
    private String tagHTML = "";

    public Ex1(String tagHTML) {
        this.tagHTML = tagHTML;
        System.out.print("<" + tagHTML + ">");
    }

    public void bodyHTML(String bodyHTML) {
        System.out.println(bodyHTML);
    }

    public void close() throws Exception {
        System.out.print("</" + tagHTML + ">");
    }

    public static void main(String[] args) throws Exception {
        try (
                Ex1 tag1 = new Ex1("p");
                Ex1 tag2 = new Ex1("div")
        ) {
            tag1.bodyHTML("testowa treść");
            tag2.bodyHTML("testowa treść");
        }

    }
}
