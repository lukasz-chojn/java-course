/*
Napisz program wykorzystując set i get
 */

package Lesson2;

public class Ex1 {

    private float cena;

    public static void main(String[] args) {
        Ex1 ex = new Ex1();
        ex.ustawCene();
        ex.pobierzCene();
    }

    private void ustawCene() {
        cena = (float) 25.34;
    }

    protected void pobierzCene() {
        System.out.println("Cena wynosi: " + cena);
    }
}
