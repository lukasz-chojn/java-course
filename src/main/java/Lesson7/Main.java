/*
Napisz dwie klasy implementujące interfejs Computation. Niech jedna z implementacji przeprowadza operację dodawania, druga mnożenia.
 */
package Lesson7;

import java.util.Scanner;

public class Main {

    static int liczba1;
    static int liczba2;

    Scanner sc = new Scanner(System.in);
    static int wynik;

    public static void main(String[] args) {
        Main n = new Main();
        Obliczenia obliczenia;

        if (n.wykonajDzialanie()) {
            obliczenia = new Mnozenie();
        } else {
            obliczenia = new Dodaj();
        }

        liczba1 = n.pobierzLiczbyOdUsera();
        liczba2 = n.pobierzLiczbyOdUsera();
        wynik = obliczenia.operacja(liczba1, liczba2);
        System.out.println(wynik);
    }


    private int pobierzLiczbyOdUsera() {
        System.out.print("Podaj liczbę: ");
        return sc.nextInt();
    }

    private boolean wykonajDzialanie() {
        System.out.println("Wciśnij \"m\", aby pomnożyć. \nKażda inna litera spowoduje wykonanie dodawania");
        sc.next().equals("m");
        return true;
    }
}
