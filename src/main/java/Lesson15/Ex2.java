/*
Napisz program, który będzie pobierał od użytkownika imiona par dopóki nie wprowadzi imienia „-”,
następnie poproś użytkownika o podanie jednego z wcześniej wprowadzonych imion i wyświetl imię odpowiadającego mu partnera.
 */
package Lesson15;

import java.util.HashMap;
import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        Ex2 p = new Ex2();
        p.getUsersNames();
        p.getUserNameToVeryfi();
        p.VeryfiNames();
    }

    Scanner sc = new Scanner(System.in);
    private String nameM = "";
    private String nameF;
    private final HashMap<String, String> couples = new HashMap<>();

    private HashMap<String, String> getUsersNames() {
        while (true) {
            System.out.print("Podaj imię męskie: ");
            nameM = sc.next();
            System.out.print("Podaj imię żeńskie: ");
            nameF = sc.next();
            if (nameM.equals("-") || nameF.equals("-")) {
                System.out.print("Zakończyłeś podawanie imion" + "\n");
                break;
            } else {
                couples.put(nameM, nameF);
            }
        }
        return couples;
    }

    private String getUserNameToVeryfi() {
        System.out.print("Podaj imię męskie, dla którego szukasz odpowiednika: ");
        nameM = sc.next();
        return nameM;
    }

    private void VeryfiNames() {
        for (String nameM : couples.keySet()) {
            String value = couples.get(nameM);
            System.out.println("Odpowiednikiem dla imienia " + nameM + " jest " + value);
        }
    }
}
