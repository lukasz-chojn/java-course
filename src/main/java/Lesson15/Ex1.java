/*
Napisz program, który będzie pobierał od użytkownika imiona.
Program powinien pozwolić użytkownikowi na wprowadzenie dowolnej liczby imion (wprowadzenie „-” jako imienia przerwie wprowadzanie).
Na zakończenie wypisz liczbę unikalnych imion.
 */
package Lesson15;

import java.util.HashSet;
import java.util.Scanner;

public class Ex1 {
    Scanner sc = new Scanner(System.in);
    String name;
    int numberNames;
    HashSet<String> names = new HashSet<>();

    public static void main(String[] args) {
        Ex1 k = new Ex1();
        k.colectingNames();
        k.displayNames();
    }

    private HashSet<String> colectingNames() {
        while (true) {
            System.out.print("Podaj imię: ");
            name = sc.next();
            if (name.equals("-")) {
                System.out.print("Zakończyłeś podawanie imion" + "\n");
                break;
            } else {
                names.add(name);
                numberNames++;
            }
        }
        return names;
    }

    private void displayNames() {

        System.out.println("Wprowadzone zostało " + numberNames + " imion, z czego unikalnych było " + names.size());
        System.out.println("Unikalne imiona to: ");
        for (String result : names) {
            System.out.println(result);
        }
    }
}
