/*
Zadeklaruj mapę, której klucze są łańcuchami znaków, a wartości dziedziczą po typie CharSequnce. Wyświetl wartości tej mapy.
 */
package Lesson13;

import java.util.Map;

public class Ex6 {
    public static void main(String[] args) {
        Ex6 p = new Ex6();
        p.createMapList();
    }

    private void createMapList() {
        Map<String, ? extends CharSequence> mapa = null;
        mapa.put("test", null);
        System.out.println(mapa);
    }
}
