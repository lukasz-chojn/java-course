/*
Utwórz zbiór generyczny liczb całkowitych, wykorzystując jako implementację klasę HashSet.
 */
package Lesson13;

import java.util.HashSet;
import java.util.Set;

public class Ex1 {
    public static void main(String[] args) {
        Ex1 k = new Ex1();
        k.createHashSet();
    }

    private void createHashSet() {
        Set<Integer> listOfNumbers = new HashSet<Integer>();
        listOfNumbers.add(5);
        listOfNumbers.add(6);
        listOfNumbers.add(124785);
    }
}
