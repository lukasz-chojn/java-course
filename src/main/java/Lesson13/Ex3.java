/*
Utwórz listę generyczną, której elementami są listy liczb całkowitych.
 */
package Lesson13;

import java.util.List;

public class Ex3 {
    public static void main(String[] args) {
        Ex3 k = new Ex3();
        k.createListObjects();
    }

    private void createListObjects() {
        List<Integer> list = null;
        list.add(5);
        list.add(12557);
        for (int x : list) {
            System.out.println(x);
        }
    }
}
