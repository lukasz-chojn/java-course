/*
Utwórz mapę generyczną, której kluczami są obiekty, a wartościami – łańcuchy znaków.
 */
package Lesson13;

import java.util.HashMap;
import java.util.Map;

public class Ex2 {
    public static void main(String[] args) {
        Ex2 k = new Ex2();
        k.createMapObjects();
    }

    private void createMapObjects() {
        Map<Object, String> map = new HashMap<Object, String>();
        Object Dom = 0;
        Object Drzewo = 0;
        map.put(Dom, "test");
        map.put(Drzewo, "Topola");
    }
}
