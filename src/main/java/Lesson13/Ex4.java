/*
Zadeklaruj zmienną typu generycznego, która pozwoli na przypisanie do niej listy typu CharSequence lub pochodnych
 */
package Lesson13;

import java.util.ArrayList;
import java.util.List;

public class Ex4 {
    public static void main(String[] args) {
        Ex4 p = new Ex4();
        p.createListChars();
    }

    private void createListChars() {
        List<? extends CharSequence> lista = new ArrayList<CharSequence>();
        lista.add(null);
        System.out.println(lista);
    }
}
