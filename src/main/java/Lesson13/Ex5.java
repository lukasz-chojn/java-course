/*
Zadeklaruj zmienną, której typem jest lista generyczna, pozwalająca na przechowanie list generycznych dowolnych typów.
 */
package Lesson13;

import java.util.List;

public class Ex5 {
    public static void main(String[] args) {
        Ex5 p = new Ex5();
        p.createList();
    }

    private void createList() {
        List<List<?>> lista = null;
        String a = "test";
        int b = 5;
        lista.add(null);
        lista.add(null);
        System.out.println(lista);
    }
}
