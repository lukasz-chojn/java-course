package Lesson21;
import java.util.*;

public class Ex2 {
    private static List<String> listOfStrings = new ArrayList<>();

    public static void main(String[] args) {
        Ex2 p = new Ex2();
        p.collectingWords();
        System.out.println("Lista przed sortowaniem");
        p.display(listOfStrings);
        p.sortingList(listOfStrings);
        System.out.println("Lista po sortowaniu");
        p.display(listOfStrings);
    }

    private void collectingWords() {
        String word = "";
        int number = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj pięć wyrazów: ");
        while (number < 5) {
            word = sc.next();
            listOfStrings.add(word);
            number++;
        }
    }

    private List<String> sortingList(List<String> listOfStrings) {
        Collections.sort(listOfStrings, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        });
        return listOfStrings;
    }

    private void display(List<String> listOfStrings) {
        for (String after : listOfStrings) {
            System.out.println(after);
        }
    }
}
