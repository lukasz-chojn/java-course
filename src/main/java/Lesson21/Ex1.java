/*
Rozszerz przykład z robotami z akapitu o klasach anonimowych o robota witającego się w innym języku np. niemieckim.
 */
package Lesson21;

public class Ex1 {
    public static class Robot {
        private final GreetingModule greetingModule;

        public Robot(GreetingModule greetingModule) {
            this.greetingModule = greetingModule;
        }

        public void saySomething() {
            greetingModule.sayHello();
        }

        public interface GreetingModule {
            void sayHello();
        }

        public static void main(String[] args) {
            Robot jan = new Robot(new GreetingModule() {
                @Override
                public void sayHello() {
                    System.out.println("dzien dobry");
                }
            });
            Robot john = new Robot(new GreetingModule() {
                @Override
                public void sayHello() {
                    System.out.println("good morning");
                }
            });
            Ex1.Robot lukasz = new Ex1.Robot(new GreetingModule() {
                @Override
                public void sayHello() {
                    System.out.println("guten morgen");
                }
            });

            jan.saySomething();
            john.saySomething();
            lukasz.saySomething();
        }
    }
}
