/*
Napisz program, który pobierze od użytkownika liczbę i wyświetli jej pierwiastek. Do obliczenia pierwiastka możesz użyć istniejącej metody java.lang.Math.sqrt().
Jeśli użytkownik poda liczbę ujemną rzuć wyjątek java.lang.IllegalArgumentException. Obsłuż sytuację, w której użytkownik poda ciąg znaków, który nie jest liczbą.
 */
package Lesson9;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Ex1 {

    static double number;
    double result;

    Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Ex1 k = new Ex1();
        k.getNumberFromUser();
        k.sQuareRoot(number);

    }

    private double getNumberFromUser() {
        while (true) {
            System.out.print("Podaj liczbę: ");
            try {
                number = sc.nextDouble();
                if (number < 0) {
                    throw new IllegalArgumentException();
                }
                break;
            } catch (InputMismatchException exception) {
                System.out.println("Się wywaliło. Podaj liczbę!");
            } catch (IllegalArgumentException f) {
                System.out.println("Podaj liczbę dodatnią. No bez jaj!");
                System.out.print("Podaj liczbę: ");

            }
            sc.next();
        }
        return number;
    }


    public double sQuareRoot(double number) {

        result = Math.sqrt(number);
        System.out.println("Wynik: " + result);
        return result;
    }

}
