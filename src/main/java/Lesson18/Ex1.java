/*
Napisz program, który poprosi użytkownika o wprowadzenie kilku imion, imiona te zapisz w liście a następnie zserializuj ją do pliku.
Napisz metodę, która odczyta ten plik i wyświetli zawartość listy na konsoli.
 */
package Lesson18;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ex1 {
    private List<String> listOfNames = new ArrayList<>();

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Ex1 p = new Ex1();
        p.collectingNames();
        p.serializedList();
        p.deserializedList();
    }

    private List<String> collectingNames() {
        int numberNames = 1;
        int maxNumber = 5;
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Podałeś: " + numberNames + " z " + maxNumber + " imion. ");
            System.out.print("Podaj imię osoby: ");
            String name = sc.next();
            listOfNames.add(name);
            numberNames++;
            if (numberNames == 6) {
                System.out.println("Podałeś już wszystkie imiona");
                break;
            }
        }
        return listOfNames;
    }

    private void serializedList() throws IOException {
        try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("test.bin"))) {
            output.writeObject(listOfNames);
        }
    }

    private void deserializedList() throws IOException, ClassNotFoundException {
        try (ObjectInputStream input = new ObjectInputStream(new FileInputStream("test.bin"))) {
            List<String> list = (List<String>) input.readObject();
            for (String savedList : list) {
                System.out.println(savedList);
            }
        }
    }
}
