/*
Napisz klasę Human, która będzie miała dwa atrybuty name typu String oraz age typu int.
Jak należałoby serializować instancje tej klasy aby zawsze poprawnie deserializować wiek (z dokładnością do roku)?
 */
package Lesson18;

import java.io.*;
import java.util.Calendar;

public class Ex2 {

}

class Human implements Serializable {
    private transient Integer age;
    private String name;

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Human man = new Human("Łukasz", 34);
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("human.bin"))) {
            outputStream.writeObject(man);
        }
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("human.bin"))) {
            Human human = (Human) inputStream.readObject();
            System.out.println(human.getName());
            System.out.println(human.getAge());
        }
    }

    public Human(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    private void readNameAndObject(ObjectInputStream inputStream) throws IOException {
        name = inputStream.readUTF();
        int yearOfBirth = inputStream.readInt();
        age = Calendar.getInstance().get(Calendar.YEAR) - yearOfBirth;
    }

    private void writeNameAndObject(ObjectOutputStream outputStream) throws IOException {
        outputStream.writeUTF(name);
        int yearOfBirth = Calendar.getInstance().get(Calendar.YEAR) - age;
        outputStream.writeInt(yearOfBirth);
    }
}
