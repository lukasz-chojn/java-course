/*
Utwórz klasę Human reprezentującą człowieka, musi posiadać atrybuty takie jak wiek, waga, wzrost, imię i płeć.
Klasa powinna także zawierać metody getAge, getWeight, getHeight, getName, isMale
 */
package Lesson12.Objects;

public class Ex1 {
    public class Human {
        private int getAge;
        private int getWeight;
        private int getHeight;
        private String getName;
        private boolean isMale;

        private void getParameter(int getAge, int getHeight, int getWeight, String getName, boolean isMale) {
            this.getAge = getAge;
            this.getWeight = getWeight;
            this.getHeight = getHeight;
            this.getName = getName;
            this.isMale = isMale;
        }
    }
}
