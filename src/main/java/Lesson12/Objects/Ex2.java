/*
Utwórz klasę reprezentującą prostokąt, musi posiadać atrybuty długość i szerokość.
Klasa powinna posiadać metody obliczające pole, obwód i długość przekątnej.
 */
package Lesson12.Objects;

public class Ex2 {
    public class Rectangle {
        private int lenght;
        private int weight;

        private int fieldCalculation() {
            int fieldResult = lenght * weight;
            return fieldResult;
        }

        private int circuitCalculation() {
            int circuitResult = 2 * lenght + 2 * weight;
            return circuitResult;
        }

        private int diagonalCalculation() {
            double aSide = (double) lenght;
            double bSide = (double) weight;
            double aSidePow = Math.pow(aSide, 2);
            double bSidePow = Math.pow(bSide, 2);
            double powSum = aSidePow + bSidePow;
            double result = Math.sqrt(powSum);
            int diagonalCalculation = (int) result;
            return diagonalCalculation;
        }
    }
}
