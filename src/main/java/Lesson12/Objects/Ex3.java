/*
Utwórz klasę o nazwie MyNumber, której jedyny konstruktor przyjmuje liczbę. Klasa powinna mieć następujące metody
MyNumber isOdd() - true jeśli atrybut jest nieparzysty,
MyNumber isEven() - true jeśli atrybut jest parzysty,
MyNumber sqrt() - pierwiastek z atrybutu,
MyNumber pow(MyNumber x) - atrybut podniesiony do potęgi x (przydatnej metody poszukaj w javadoc do klasy Math),
MyNumber add(MyNumber x) - zwraca sumę atrybutu i x opakowaną w klasę MyNumber,
MyNumber subtract(MyNumber x) - zwraca różnicę atrybutu i x opakowaną w klasę MyNumber
 */
package Lesson12.Objects;

public class Ex3 {
    public class MyNumber {
        double value;
        int number;

        public MyNumber(double value) {
            this.value = value;
        }

        private boolean isOddEven() {
            if (number % 2 == 0) {
                this.number = number;
            } else {
                this.number = number;
            }
            return true;
        }

        private double sqrt() {
            int result = 0;
            double reultSqrt = (double) Math.sqrt(result);
            return reultSqrt;
        }

        private MyNumber pow(MyNumber powx) {
            double result = (double) number;
            return new MyNumber(Math.pow(result, powx.value));
        }

        private MyNumber add(MyNumber anotherValue) {
            return new MyNumber(number + anotherValue.value);
        }

        private MyNumber subtract(MyNumber anotherValue) {
            return new MyNumber(number - anotherValue.value);
        }
    }
}
