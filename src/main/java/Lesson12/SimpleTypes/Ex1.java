/*
Jeśli w klasie Human użyłeś typów prostych zamień je na odpowiadające im klasy (int => Integer)
 */
package Lesson12.SimpleTypes;

public class Ex1 {
    public static void main(String[] args) {

    }

    public class Human {
        private Integer getAge;
        private Integer getWeight;
        private Integer getHeight;
        private String getName;
        private boolean isMale;

        private void getParameter(Integer getAge, Integer getHeight, Integer getWeight, String getName, boolean isMale) {
            this.getAge = getAge;
            this.getWeight = getWeight;
            this.getHeight = getHeight;
            this.getName = getName;
            this.isMale = isMale;
        }
    }
}
