/*
Napisz program, który pobierze od użytkownika liczbę całkowitą a następnie wyświetli jej binarną reprezentację na ekranie
 */
package Lesson12.SimpleTypes;

import java.util.Scanner;

public class Ex2 {
    int number;
    Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Ex2 m = new Ex2();
        m.getNumber();
        m.printBinary();
    }

    private int getNumber() {
        System.out.print("Podaj liczbę, który chcesz wyświetlić w postaci binarnej: ");
        number = sc.nextInt();
        return number;
    }

    private void printBinary() {
        System.out.println("Liczba " + number + " w systemie binarnym to: " + Integer.toBinaryString(number));
    }
}
