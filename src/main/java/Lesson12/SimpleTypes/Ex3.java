/*
Napisz program, który pobierze od użytkownika liczbę całkowitą N reprezentującą długość tablicy
a następnie poprosi o N kolejnych liczb uzupełniając nimi wcześniej stworzoną tablicę.
Wyświetl na konsoli tablicę posortowaną w kolejności od najmniejszej do największej liczby
 */
package Lesson12.SimpleTypes;

import java.util.Arrays;
import java.util.Scanner;

public class Ex3 {
    static int arraySize;
    static int[] array;
    Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Ex3 o = new Ex3();
        arraySize = o.arraySize();
        o.createArray();
        o.sortAndDisplayArray(array);
    }

    private int arraySize() {
        System.out.print("Podaj rozmiar tablicy: ");
        arraySize = sc.nextInt();
        return arraySize;
    }

    private int[] createArray() {
        array = new int[arraySize];
        System.out.print("Podaj elementy, które będą dodane do tablicy, ");
        for (int i = 0; i < array.length; i++) {
            System.out.print((i + 1) + " element tablicy: ");
            array[i] = sc.nextInt();
        }
        return array;
    }

    private void sortAndDisplayArray(int[] array) {
        Arrays.sort(array);
        System.out.print("Podałeś następujące liczby, które zostały posortowane od najmniejszej do największej: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
