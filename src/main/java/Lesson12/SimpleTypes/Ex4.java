/*
Napisz program, który pobierze od użytkownika łańcuch znaków i wyświetli na konsoli jego:
- długość,
- informację czy jest to palindrom czy nie
- oraz jego odwróconą wartość
 */
package Lesson12.SimpleTypes;

import java.util.Scanner;

public class Ex4 {
    String word;
    final Scanner sc = new Scanner(System.in);

    private String getWord() {
        System.out.print("Podaj wyraz, który chcesz sprawdzić: ");
        word = sc.next();
        return word;
    }

    private String checkPalindrome() {
        String reverse = new StringBuffer(word).reverse().toString();
        if (word.equals(reverse)) {
            System.out.print("Wskazany wyraz jest palindromem" + "\n");
        } else {
            System.out.println("Wskazany wyraz nie jest palindromem" + "\n");
        }
        return reverse;
    }

    private void checkLenght() {
        int lenght;
        lenght = word.length();
        System.out.print("Wpisany wyraz ma długość " + lenght + " znaków" + "\n");
    }

    private void reverseWord() {
        String reverse = new StringBuffer(word).reverse().toString();
        System.out.print("Wskazany wyraz pisany od tyłu brzmi: " + reverse);
    }

    public static void main(String[] args) {
        Ex4 h = new Ex4();
        h.getWord();
        h.checkLenght();
        h.checkPalindrome();
        h.reverseWord();
    }
}
