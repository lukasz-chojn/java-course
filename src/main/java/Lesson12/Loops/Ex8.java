/*
Do klasy ArrayFactory dodaj metodę zwracającą macierz jednostkową (jedynki “na przekątnej”)
 */
package Lesson12.Loops;

public class Ex8 {

    static class ArrayFactory {
        int x = 5;
        int[] oneDimension = new int[x];
        int[][] twoDimension = new int[x][x];

        public void arrayFactory(int x) {
            this.x = x;
        }

        public int[] oneDimension() {
            return oneDimension;
        }

        public int[][] twoDimension() {
            return twoDimension;
        }

        public int[][] identifyMatrix() {
            int[][] matrix = twoDimension();
            for (int i = 0; i < x; i++) {
                for (int j = 0; j < x; j++) {
                    if (i == j) {
                        matrix[i][j] = 1;
                    } else {
                        matrix[i][j] = 0;
                    }
                }
            }
            return matrix;
        }
    }

    public static void main(String[] args) {
        ArrayFactory j = new ArrayFactory();
        j.arrayFactory(5);
        j.oneDimension();
        j.twoDimension();
        j.identifyMatrix();
    }
}
