/*
Utwórz metodę, która pobierze liczbę oraz zwróci ją w formie binarnej (2 => “10”, 4 => “100”, 5 => “101”, itd.).
 */
package Lesson12.Loops;

public class Ex6 {
    int[][] numbers = {{2, 4, 5}, {10, 100, 101}};

    private void numbersToBinary() {
        for (int i = 0; i <= numbers.length; i++) {
            System.out.print(numbers[0][i] + "=>" + numbers[1][i] + "\n");
        }
    }

    public static void main(String[] args) {
        Ex6 l = new Ex6();
        l.numbersToBinary();
    }
}
