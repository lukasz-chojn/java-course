/*
Utwórz metodę, która pobierze liczbę i wypisze każdy znak w osobnej linii
zaczynając od ostatniej cyfry (np. dla liczby 123 będą to trzy linie z 3, 2 i 1)
 */
package Lesson12.Loops;

public class Ex4 {
    public static void main(String[] args) {
        Ex4 k = new Ex4();
        k.returnNumbers();
    }

    int numbers[] = {1, 2, 3};

    private void returnNumbers() {
        for (int i = numbers.length - 1; i >= 0; i--) {
            System.out.println(numbers[i]);
        }
    }
}
