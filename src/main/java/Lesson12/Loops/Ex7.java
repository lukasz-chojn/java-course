/*
Utwórz metodę, pobierającą łańcuch znaków, która sprawdzi czy jest on palindromem.
Np. “kajak” jest palindromem (to samo czytane “od przodu i od tyłu”) jednak “kotek” już nie
 */
package Lesson12.Loops;

import java.util.Scanner;

public class Ex7 {
    String word;
    final Scanner sc = new Scanner(System.in);

    private String getWord() {
        System.out.print("Podaj wyraz, który chcesz sprawdzić: ");
        word = sc.next();
        return word;
    }

    private void checkPalindrome() {
        String reverse = new StringBuffer(word).reverse().toString();
        if (word.equals(reverse)) {
            System.out.print("Wskazany wyraz jest palindromem");
        } else {
            System.out.println("Wskazany wyraz nie jest palindromem");
        }
    }

    public static void main(String[] args) {
        Ex7 k = new Ex7();
        k.getWord();
        k.checkPalindrome();
    }
}
