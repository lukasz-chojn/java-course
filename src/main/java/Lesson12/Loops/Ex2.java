/*
Jeśli w zadaniu 1. użyłeś pętli for przerób tą metodę na while (lub odwrotnie)
 */
package Lesson12.Loops;

public class Ex2 {
    public static void main(String[] args) {
        Ex2 b = new Ex2();
        b.displayNumbers();
    }

    public void displayNumbers() {
        int x = 0;
        int number = 5;
        while (x <= number) {
            System.out.print(x + " ");
            x = x + 1;
        }
    }
}
