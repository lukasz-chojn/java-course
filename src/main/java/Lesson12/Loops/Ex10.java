/*
Napisz metodę, która pobierze tablicę liczb całkowitych i posortuj ją w kolejności od najmniejszej do największej liczby
 */
package Lesson12.Loops;

import java.util.Arrays;

public class Ex10 {
    int[] arrayNumbers = {20, 13, 18, 25, 2, 9};

    public static void main(String[] args) {
        Ex10 p = new Ex10();
        p.sortArray();
    }

    private void sortArray() {
        Arrays.sort(arrayNumbers);
        for (int i = 0; i < arrayNumbers.length; i++) {
            System.out.print(arrayNumbers[i] + " ");
        }
    }
}
