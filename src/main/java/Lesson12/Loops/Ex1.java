/*
Utwórz metodę pobierającą dodatnią liczbę całkowitą X, która wyświetli na ekranie liczby od zera do X
 */
package Lesson12.Loops;

public class Ex1 {
    int[] numbers = {0, 1, 2, 3, 4, 5};

    public static void main(String[] args) {
        Ex1 b = new Ex1();
        b.displayNumbers();
    }

    public void displayNumbers() {
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
    }
}
