/*
Utwórz metodę, która jako argument pobierze obiekt klasy String
i zwróci “odwrócony” argument. Na przykład “pies” przekształci w “seip”
 */
package Lesson12.Loops;

public class Ex5 {
    String[] word = {"p", "i", "e", "s"};

    public static void main(String[] args) {
        Ex5 l = new Ex5();
        l.reverseWord();
    }

    private void reverseWord() {
        for (int i = word.length - 1; i >= 0; i--) {
            System.out.print(word[i]);
        }
    }
}
