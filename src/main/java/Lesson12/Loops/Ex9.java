/*
Napisz metodę, która pobierze tablicę liczb całkowitych i wyświetli ją w postaci “[liczba, liczba, liczba]
 */
package Lesson12.Loops;

public class Ex9 {
    int[] arrayNumbers = {2, 5, 8, 20, 13};

    public static void main(String[] args) {
        Ex9 k = new Ex9();
        k.splitNumbers();
    }

    private void splitNumbers() {
        boolean lastNumber;
        System.out.print("[");
        for (int i = 0; i < arrayNumbers.length; i++) {
            System.out.print(arrayNumbers[i]);
            lastNumber = i == arrayNumbers.length - 1;
            if (lastNumber) {
                System.out.print("]");
            } else {
                System.out.print(", ");
            }
        }
    }
}
