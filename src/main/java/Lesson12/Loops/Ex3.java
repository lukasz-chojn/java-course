/*
Napisz klasę ArrayFactory, która w konstruktorze pobierze liczbę całkowitą X większą od zera. Klasa powinna mieć 2 metody:
int[] oneDimension - zwróci instancję tablicy jednowymiarowej o długości X
int[][] twoDimension - zwróci instację tablicy dwuwymiarowej gdzie liczba wierszy i liczba kolumn równa się X
 */
package Lesson12.Loops;

public class Ex3 {
    public static void main(String[] args) {
        ArrayFactory v = new ArrayFactory();
        v.oneDimension();
        v.twoDimension();
    }

    static class ArrayFactory {
        int x = 5;
        int[] oneDimension = new int[x];
        int[][] twoDimension = new int[x][x];

        public void arrayFactory() {
            this.x = x;
        }

        public int[] oneDimension() {
            return oneDimension;
        }

        public int[][] twoDimension() {
            return twoDimension;
        }
    }
}
