/*
Napisz metodę pobierającą trójelementową tablicę liczb, która zwróci tablicę zawierającą te same elementy w odwróconej kolejności
 */
package Lesson12.Tables;

import java.util.Scanner;

public class Ex2 {
    static String firstLetter;
    static String secondLetter;
    static String thirdLetter;

    public static void main(String[] args) {
        Ex2 i = new Ex2();
        i.getLetters();
        i.printLetters(firstLetter, secondLetter, thirdLetter);

    }

    public String[] getLetters() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Podaj pierwszą literę: ");
        firstLetter = sc.next();
        System.out.print("Podaj drugą literę: ");
        secondLetter = sc.next();
        System.out.print("Podaj trzecią literę: ");
        thirdLetter = sc.next();
        return new String[3];
    }

    public void printLetters(String firstLetter, String secondLetter, String thirdLetter) {
        String letters[] = {firstLetter, secondLetter, thirdLetter};
        System.out.print("podałeś następujące litery: ");
        for (int i = letters.length - 1; i >= 0; i--) {
            System.out.print(" " + letters[i]);
        }
    }

}
