/*
Napisz metodę, która zwróci tablicę String[] zawierającą pierwsze 5 liter alfabetu
 */
package Lesson12.Tables;

public class Ex1 {

    public static void main(String[] args) {
        Ex1 i = new Ex1();
        i.printLetters();

    }

    public void printLetters() {
        String a = "a";
        String b = "b";
        String c = "c";
        String d = "d";
        String e = "e";
        String f = "f";
        String letters[] = {a, b, c, d, e, f};
        System.out.print("Litery: ");
        for (int i = 0; i < letters.length; i++) {
            System.out.print(" " + letters[i]);
        }
    }
}
