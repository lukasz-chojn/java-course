package Lesson12.Interface;

public interface Figure {
    public int getArea();

    public int getPerimeter();

    public String getType();
}
