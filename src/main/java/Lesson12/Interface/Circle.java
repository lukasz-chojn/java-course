package Lesson12.Interface;

public class Circle implements Figure {
    int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    public int getPerimeter() {
        return (int) (2 * Math.PI * radius);
    }

    public int getArea() {
        return (int) (Math.PI * radius * radius);
    }

    @Override
    public String getType() {
        return "Koło";
    }
}
