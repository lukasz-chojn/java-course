package Lesson12.Interface;

public class Triangle implements Figure {
    int a;
    int b;
    int h;

    public Triangle(int a, int b, int h) {
        this.a = a;
        this.b = b;
        this.h = h;
    }

    public int getPerimeter() {
        return a + b + h;
    }

    public int getArea() {
        return a * b / 2;
    }

    @Override
    public String getType() {
        return "Trójkąt";
    }
}
