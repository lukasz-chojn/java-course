package Lesson12.Interface;

public class Rectangle implements Figure {
    int a;
    int b;

    public Rectangle(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public int getPerimeter() {
        return 2 * a + 2 * b;
    }

    @Override
    public int getArea() {
        return a * b;
    }

    @Override
    public String getType() {
        return "Prostokąt";
    }
}
