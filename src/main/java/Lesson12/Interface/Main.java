/*
Stwórz interfejs Figure. Interfejs powinien zawierać metody getPerimeter (zwracającą obwód) oraz getArea (zwracającą powierzchnię).
Następnie utwórz klasy Circle, Triangle i Rectangle, niech każda z klas implementuje interfejs Figure.
Napisz program, który pobierze od użytkownika:
- długość promienia koła,
- 2 długości boków trójkąta prostokątnego (boki przy kącie prostym),
- długość boków prostokąta.
Utworzy instancje tych obiektów i umieści je w tablicy Figure[].
Następnie iterując po obiektach wyświetl pole oraz obwód aktualnego obiektu.
 */
package Lesson12.Interface;

import java.util.Scanner;

public class Main {
    static Figure[] figures = new Figure[3];
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Podaj wartość promienia: ");
        figures[0] = new Circle(sc.nextInt());
        System.out.print("Podaj dwóch boków prostokąta: ");
        figures[1] = new Rectangle(sc.nextInt(), sc.nextInt());
        System.out.print("Podaj dwóch boków trójkąta i jego wysokość: ");
        figures[2] = new Triangle(sc.nextInt(), sc.nextInt(), sc.nextInt());

        for (Figure figure : figures) {
            System.out.print("Figura: " + figure.getType() + "\n");
            System.out.print("Pole " + figure.getArea() + "\n");
            System.out.print("Obwód " + figure.getPerimeter() + "\n");
        }
    }
}
