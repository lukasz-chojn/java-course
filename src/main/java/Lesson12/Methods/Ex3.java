/*
Napisz metodę, która jako argument przyjmuje 2 liczby i wypisuje ich sumę, różnicę i iloczyn
 */
package Lesson12.Methods;

import java.util.Scanner;

public class Ex3 {

    int number1;
    int number2;

    public static void main(String[] args) {
        Ex3 j = new Ex3();
        j.getNumbers();
        j.Calculation();

    }

    private int[] getNumbers() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj pierwszą liczbę i naciśnij <ENTER>");
        number1 = sc.nextInt();
        System.out.println("Podaj drugą liczbę i naciśnij <ENTER>");
        number2 = sc.nextInt();
        return new int[]{number1, number2};
    }

    private void Calculation() {
        int i;
        int[] numbers = {number1, number2};
        int sumResult = 0;
        int diffResult = 0;
        int multiplyResult = 0;
        for (i = 0; i < numbers.length; i++) {

            sumResult = sumResult + numbers[i];
            diffResult = diffResult - numbers[i];
            multiplyResult = multiplyResult * numbers[i];

            System.out.println("Suma: " + sumResult + ", Różnica: " + diffResult + ", Mnożenie: " + multiplyResult);
        }
    }

}
