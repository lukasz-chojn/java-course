/*
Napisz metodę, która jako argument przyjmuje liczbę i zwraca jej pierwiastek kwadratowy
 */
package Lesson12.Methods;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Ex7 {
    static double number;
    static double result;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbę i naciśnij <ENTER>");
        try {
            number = sc.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Musisz podać liczbę");
            System.exit(0);
        }
        result = Math.sqrt(number);
        System.out.println("Liczba " + number + " spierwastkowana pierwiastkiem kwadratowym wynosi: " + result);
    }
}
