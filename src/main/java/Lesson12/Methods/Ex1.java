/*
Napisz metodę, która zwróci Twój aktualny wiek.
 */
package Lesson12.Methods;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class Ex1 {
    public static void main(String[] args) {
        Ex1 k = new Ex1();
        k.CalculateDate();
    }

    private void CalculateDate() {
        LocalDateTime birthDate = LocalDateTime.of(1984, 11, 23, 21, 30, 00);
        LocalDateTime today = LocalDateTime.now();

        long resultYears = ChronoUnit.YEARS.between(birthDate, today);
        long resultMonths = ChronoUnit.MONTHS.between(birthDate, today);
        long resultDays = ChronoUnit.DAYS.between(birthDate, today);
        long resultHours = ChronoUnit.HOURS.between(birthDate, today);
        long resultMinutes = ChronoUnit.MINUTES.between(birthDate, today);
        long resultSeconds = ChronoUnit.SECONDS.between(birthDate, today);

        System.out.println("Twój wiek wynosi: " + resultYears + " lat(a),\nna które składają się: " + "\n" + resultMonths + " miesięc(y) lub " + resultDays + " dni");
        System.out.println("Przeżyłeś już: " + resultHours + " godzin lub " + resultMinutes + " minut lub " + resultSeconds + " sekund");
    }
}
