/*
Napisz metodę, która jako argument przyjmie trzy liczby.
Metoda powinna zwrócić true jeśli z odcinków o długości przekazanych w argumentach można zbudować trójkąt prostokątny
 */
package Lesson12.Methods;

import java.util.Scanner;

public class Ex8 {
    double a;
    double b;
    double c;
    double aResult;
    double bResult;
    double cResult;
    double sidesResult;
    double[] sides;

    public static void main(String[] args) {
        Ex8 h = new Ex8();
        h.getTriangleSides();
        h.triangleCalculation();
        h.isRectangular();

    }

    private double[] getTriangleSides() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Podaj długość boku a: ");
        a = sc.nextDouble();
        System.out.print("Podaj długość boku b: ");
        b = sc.nextDouble();
        System.out.print("Podaj długość boku c: ");
        c = sc.nextDouble();
        return sides;
    }

    private double[] triangleCalculation() {
        aResult = Math.pow(a, 2);
        bResult = Math.pow(b, 2);
        cResult = Math.pow(c, 2);
        sidesResult = aResult + bResult;
        return sides;
    }

    private void isRectangular() {
        if (sidesResult == cResult) {
            System.out.println("Trójkąt jest prostokątny");
        } else {
            System.out.println("Trójkąt nie jest prostokątny");
        }
    }
}
