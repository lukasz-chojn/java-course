/*
Napisz metodę, która zwróci Twoje imię
 */
package Lesson12.Methods;

import java.util.Scanner;

public class Ex2 {
    Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Ex2 n = new Ex2();
        n.getName();
    }

    private String getName() {
        System.out.print("Podaj swoje imię");
        String name = sc.next();
        System.out.println("Nazywasz się: " + name);
        return name;
    }
}
