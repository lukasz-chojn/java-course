/*
Napisz metodę, która jako argument przyjmuje liczbę i zwraca go podniesionego do 3 potęgi
 */
package Lesson12.Methods;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Ex6 {
    static double number;
    static double result;
    static int pow = 3;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbę i naciśnij <ENTER>");
        try {
            number = sc.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Musisz podać liczbę");
            System.exit(0);
        }
        result = Math.pow(number, pow);
        System.out.println("Liczba " + number + " podniesiona do potęgi " + pow + " wynosi: " + result);
    }
}
