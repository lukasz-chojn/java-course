/*
Napisz metodę, która jako argument przyjmuje liczbę i zwraca true jeśli liczba jest podzielna przez 3 i przez 5
 */
package Lesson12.Methods;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Ex5 {
    static int number;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbę i naciśnij <ENTER>");
        try {
            number = sc.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Musisz podać liczbę");
            System.exit(0);
        }
        if (number % 3 == 0 && number % 5 == 0) {
            System.out.print("Liczba jest podzielna przez 3 i 5");
        } else {
            System.out.print("Liczba nie jest podzielna przez 3 i 5");

        }
    }
}
