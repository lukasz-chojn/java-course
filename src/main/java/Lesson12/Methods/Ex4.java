/*
Napisz metodę, która jako argument przyjmuje liczbę i zwraca true jeśli liczba jest parzysta
 */
package Lesson12.Methods;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Ex4 {
    static int number;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbę i naciśnij <ENTER>");
        try {
            number = sc.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Musisz podać cyfrę lub liczbę");
            System.exit(0);
        }
        if (number % 2 == 0) {
            System.out.print("Liczba jest parzysta");
        } else {
            System.out.print("Liczba nie jest parzysta");

        }
    }
}

