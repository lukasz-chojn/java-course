/*
sprawdza czy numer domu jest w formacie numer\numer.
Poprawnym numerem jest 123\2A, 24B\3 czy 12\5, ale już numer abc\cba nie
 */
package Lesson24;

import org.junit.Test;

import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Ex2 {

    @Test
    public void shouldValidateHouseNumber() {
        Pattern pattern = Pattern.compile("\\d+[A-Za-z]?+\\\\+\\d+[A-Za-z]?");
        assertTrue(pattern.matcher("123\\2A").matches());
        assertTrue(pattern.matcher("24B\\3").matches());
        assertTrue(pattern.matcher("12\\5").matches());
        assertFalse(pattern.matcher("abc\\cba").matches());
        assertFalse(pattern.matcher("12").matches());
    }
}
