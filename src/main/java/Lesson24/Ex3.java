/*
sprawdza czy użytkownik wprowadził poprawną nazwę miasta. Na przykład Wrocław, Zielona Gora czy Bielsko-Biala jest ok, jednak Ptysiow123 już nie.
Dla uproszczenia załóżmy, że żadna nazwa miejscowości nie zwiera polskich znaków.
 */
package Lesson24;

import org.junit.Test;

import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Ex3 {

    @Test
    public void shouldValidateCityName() {
        Pattern pattern = Pattern.compile("\\w+[-]?+[\\s]?\\w+");
        assertTrue(pattern.matcher("Wroclaw").matches());
        assertTrue(pattern.matcher("Zielona Gora").matches());
        assertTrue(pattern.matcher("Bielsko-Biala").matches());
        assertFalse(pattern.matcher("wroclaw").matches());
        assertFalse(pattern.matcher("Ptysiow123").matches());
    }
}
