/*
Sprawdza czy liczba zmiennoprzecinkowa podana przez użytkownika ma poprawny format.
Na przykład liczba 123,2341515132135 czy -10 są poprawne ale 18-12 czy 123, już nie
 */
package Lesson24;

import org.junit.Test;

import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Ex1 {

    @Test
    public void shouldValidateFloatNumber() {
        Pattern pattern = Pattern.compile("-*+\\d+(,+\\d+)*");
        assertTrue(pattern.matcher("123,2341515132135").matches());
        assertTrue(pattern.matcher("-10").matches());
        assertFalse(pattern.matcher("18-12").matches());
        assertFalse(pattern.matcher("123,").matches());
    }
}
