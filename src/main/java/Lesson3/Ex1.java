/*

Napisz program, który wyświetli trzy różne zdania opisujące aktualną pogodę.
W czwartej linii wypisz sumaryczną długość trzech poprzednich zdań.

 */
package Lesson3;

public class Ex1 {
    public static void main(String[] args) {
        String wiosna = "Kwiatki kwitną, słońce świeci";
        String lato = "Gorąco, upał, ciepło";
        String jesien = "Pada, chłodem wieje";
        int dlugosc;

        dlugosc = wiosna.length() + lato.length() + jesien.length();
        System.out.println(dlugosc);
    }
}
